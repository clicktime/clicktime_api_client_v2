// Test API endpoints directly.

var supertest = require('supertest');
var api_v2 = supertest('http://clicktime-labs.herokuapp.com/API/2.0')

var headers = {
  'Accept': 'application/json',
}

var queryParams = {
  companyId: '2WPZF0iRbtls',
  userId: '2fypIz74Tsws'
}

var email = '';
var password = '';

// describe('GET 1.3/session', function(){
//   it('should redirect to resource', function(done){
//     api_v1
//     .get('/session')
//     .auth(email, password)
//     .set(headers)
//     .expect('Content-Type', /json/)
//     .expect(200, done)
//   })
//   it('should retrieve session info without error', function(done){
//     api_v1
//     .get('/session')
//     .auth(email, password)
//     .set(headers)
//     .redirects(2)
//     .expect('Content-Type', /json/)
//     .expect(function(res) {
//       res.body.UserID = queryParams.CompanyID;
//     })
//     .expect(200, {
//       CompanyID: queryParams.CompanyID.toUpperCase()
//     }, done)
//   });
// });

// describe('GET 2.0/session', function(){
//   it('redirects to resource', function(done){
//     api_v2
//     .get('/session')
//     .auth(email, password)
//     .set(headers)
//     // .redirects(2)
//     .expect('Content-Type', /json/)
//     .expect(200, done)
//   });
// });

describe('GET /Companies', function(){
  it('redirects to resource', function(done){
    api_v2
    .get('/Companies/2WPZF0iRbtls')
    .auth(email, password)
    .set(headers)
    .redirects(1)
    .expect('Content-Type', /json/)
    .expect(200)
    .end(done)
  });

  it('should retrieve my company without error', function(done) {
    api_v2
    .get('/Companies/2WPZF0iRbtls')
    .auth(email, password)
    .set(headers)
    .redirects(1)
    .expect('Content-Type', /json/)
    .expect(function(res) {
      res.body.id = queryParams.CompanyID;
    })
    .expect(200, {
      'CompanyID': '2WPZF0iRbtls',
    }, done)
  });
});

describe('GET /Companies', function(){
  it('redirects to resource', function(done){
    api_v2
    .get('/Companies/2WPZF0iRbtls')
    .auth(email, password)
    .set(headers)
    .redirects(1)
    .expect('Content-Type', /json/)
    .expect(200)
    .end(done)
  });

  it('should retrieve my company without error', function(done) {
    api_v2
    .get('/Companies/2WPZF0iRbtls')
    .auth(email, password)
    .set(headers)
    .redirects(1)
    .expect('Content-Type', /json/)
    .expect(function(res) {
      res.body.id = queryParams.CompanyID;
    })
    .expect(200, {
      'CompanyID': '2WPZF0iRbtls',
    }, done)
  });
});
