module.exports = {
  uniqueKey: new Date().getTime().toString(),
  SUCCESS_CODES: [ 200, 201, 204 ],
  should: require('chai').should(),
  api: require('../index')( {
    credentials: {
      email: process.env.EMAIL,
      password: process.env.PASSWORD,
      xBrowser: 'Test'
    },
    token: process.env.token,
    debug: process.env.DEBUG_CLICKTIME
  } )
}
