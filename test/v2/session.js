var h = require('../helper')
var SUCCESS_CODES = h.SUCCESS_CODES
var should = h.should
var api = h.api.v2

// *** No session stuff in v2 api?
// describe( 'v2/Session', function() {
//   describe( '.get()', function() {
//     it( 'should retrieve Session info without error', function( done ) {
//       api.session.get( function( err, body, response ) {
//         should.exist( response )
//         should.exist( response.statusCode )
//         SUCCESS_CODES.should.include( response.statusCode )
//         BAD_HTTP.should.not.include( response.statusCode )
//         should.exist( body.CompanyID )
//         should.exist( body.UserID )
//         done()
//       } )
//     } )
//   } )
// } )
