var h = require('../helper')
var _ = require('underscore')
var SUCCESS_CODES = h.SUCCESS_CODES
var should = h.should
var api = h.api.v2

describe( 'v2/Reports', function() {
  let jobIDs = null;
  let taskIDs = null;
  let userIDs = null;
  describe( '.timeEntries()', function() {
    it( 'should retrieve a report without error', function( done ) {
      api.report.timeEntries( {} ) .then ( function ( response ) {
        let data = response.data;
        should.exist( data )
        jobIDs = _.unique(_.pluck(data, 'JobID'))
        taskIDs = _.unique(_.pluck(data, 'TaskID'))
        userIDs = _.unique(_.pluck(data, 'UserID'))
        done()
      } ).catch((ex) => {
          done( ex )
      } )
    } )
    it( 'should filter a report by jobs without error', function( done ) {
      let ids = [ jobIDs[0], jobIDs[1] ];
      api.report.timeEntries( { JobID: ids } ) .then ( function ( response ) {
        let data = response.data;
        should.exist( data )
        should.equal(ids.length, _.intersection(_.unique(_.pluck(data, 'JobID')), ids).length)
        done()
      } ).catch((ex) => {
          done( ex )
      } )
    } )
    it( 'should filter a report by tasks without error', function( done ) {
      let ids = [ taskIDs[0], taskIDs[1] ];
      api.report.timeEntries( { TaskID: ids } ) .then ( function ( response ) {
        let data = response.data;
        should.exist( data )
        should.equal(ids.length, _.intersection(_.unique(_.pluck(data, 'TaskID')), ids).length)
        done()
      } ).catch((ex) => {
          done( ex )
      } )
    } )
    it( 'should filter a report by users without error', function( done ) {
      let ids = [ userIDs[0], userIDs[1] ];
      api.report.timeEntries( { UserID: ids } ) .then ( function ( response ) {
        let data = response.data;
        should.exist( data )
        should.equal(ids.length, _.intersection(_.unique(_.pluck(data, 'UserID')), ids).length)
        done()
      } ).catch((ex) => {
          done( ex )
      } )
    } )
  } )
} )
