var h = require('../helper')
var SUCCESS_CODES = h.SUCCESS_CODES
var should = h.should
var api = h.api.v2

describe( 'v2/Companies', function() {
  describe( '.get()', function() {
    it( 'should retrieve the Company without error', function( done ) {
      api.company.get( ).then( function( response ) {
        let data = response.data;
        should.exist( data )
        should.exist( data.ID )
        done()
      } ).catch((ex) => {
          done( ex )
      } )
    } )
  } )
} )
