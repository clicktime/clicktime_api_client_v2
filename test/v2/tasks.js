var h = require('../helper')
var SUCCESS_CODES = h.SUCCESS_CODES
var should = h.should
var api = h.api.v2
var uniqueKey = h.uniqueKey

describe( 'v2/Tasks', function() {
  var taskFixture = null
  describe( '.getAll()', function() {
    it( 'should retrieve Tasks without error', function( done ) {
      api.task.getAll( { IsActive: true } ) .then ( function ( response ) {
        let data = response.data;
        should.exist( data )
        data.length.should.be.above( 0 )
        should.exist( data[0].ID )
        taskFixture = data[0]
        done()
      } ).catch((ex) => {
          done( ex )
      } )
    } )
  } )
  describe( '.get()', function() {
    it( 'should retrieve a Task without error', function( done ) {
      api.task.get( taskFixture.ID ) .then ( function ( response ) {
        let data = response.data;
        should.exist( data )
        should.exist( data.ID )
        data.ID.should.equal( taskFixture.ID )
        done()
      } ).catch((ex) => {
          done( ex )
      } )
    } )
  } )
} )
