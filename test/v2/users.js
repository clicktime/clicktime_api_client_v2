var h = require('../helper')
var SUCCESS_CODES = h.SUCCESS_CODES
var should = h.should
var api = h.api.v2
var uniqueKey = h.uniqueKey

describe( 'v2/Users', function() {
  var userFixture = null
  var ids = null;
  describe( '.getAll()', function() {
    it( 'should retrieve Users without error', function( done ) {
      api.user.getAll( { IsActive: true } ) .then ( function ( response ) {
        let data = response.data;
        should.exist( data )
        should.exist( data[0].ID )
        userFixture = data[0]
        ids = [data[0].ID, data[1].ID]
        done()
      } ).catch((ex) => {
          done( ex )
      } )
    } )
  } )
  describe( '.getAll({ids})', function() {
    it( 'should retrieve Users without error', function( done ) {
      api.user.getAll( { ID: ids } ) .then ( function ( response ) {
        let data = response.data;
        should.exist( data )
        should.equal( ids.length, data.length )
        done()
      } ).catch((ex) => {
          done( ex )
      } )
    } )
  } )
  describe( '.get()', function() {
    it( 'should retrieve a User without error', function( done ) {
      api.user.get( userFixture.ID ) .then ( function ( response ) {
        let data = response.data;
        should.exist( data )
        should.exist( data.ID )
        data.ID.should.equal( userFixture.ID )
        done()
      } ).catch((ex) => {
          done( ex )
      } )
    } )
  } )
  describe( '.patch()', function() {
    // but why do users have sync ids?
    it( 'should change a Users name without error', function( done ) {
      var updates = { Name: 'Test' + uniqueKey }
      api.user.patch( userFixture.ID, updates ) .then ( function ( response ) {
        let data = response.data;
        should.exist( data )
        should.exist( data.ID )
        data.Name.should.equal( updates.Name )
        done()
      } ).catch((ex) => {
          done( ex )
      } )
    } )
  } )
} )
