var h = require('../helper')
var SUCCESS_CODES = h.SUCCESS_CODES
var should = h.should
var api = h.api.v2
var uniqueKey = h.uniqueKey

describe( 'v2/Labels', function() {
  var labelFixture = null

  describe( '.getAll()', function() {
    it( 'should retrieve Labels without error', function( done ) {
      api.label.getAll( { IsActive: true } ) .then ( function ( response ) {
        let data = response.data;
        should.exist( data )
        data.length.should.be.above( 0 )
        should.exist( data[0].Name )
        done()
      } ).catch((ex) => {
          done( ex )
      } )
    } )
  } )
  describe( '.post()', function() {
    it( 'should create a Label without error', function( done ) {
      var fields = {
        Name: 'Test Label ' + uniqueKey
      }
      api.label.post(  fields ) .then ( function ( response ) {
        let data = response.data;
        should.exist( data )
        should.exist( data.Name )
        data.Name.should.equal( fields.Name )
        labelFixture = data
        done()
      } ).catch((ex) => {
          done( ex )
      } )
    } )
  } )

  describe( 'v2/LabelEvents', function() {
    var labelEventFixture = null

    describe( '.post()', function() {
      it( 'should create a LabelEvent without error', function( done ) {
        api.report.timeEntries( {} ).then ( function ( response ) {
        let data = response.data;
          var timeEntryID = data[0].ID
          var fields = {
            EntityType: 'time',
            EntityIDs: timeEntryID,
            LabelID: labelFixture.ID
          }
          api.labelEvent.post(  fields ) .then ( function ( response ) {
        let data = response.data;
            should.exist( data )
            should.exist( data.ID )
            data.LabelID.should.equal( labelFixture.ID )
            labelEventFixture = data
            done()
          } ).catch((ex) => {
              done( ex )
          } )
        } ).catch((ex) => {
            done( ex )
        } )
      } )
    } )
    describe( '.getAll()', function() {
      it( 'should retrieve LabelEvents without error', function( done ) {
        api.labelEvent.getAll( { LabelID: labelFixture.ID } ) .then ( function ( response ) {
        let data = response.data;
          should.exist( data )
          data.length.should.be.above( 0 )
          done()
        } ).catch((ex) => {
            done( ex )
        } )
      } )
    } )
    describe( '.delete()', function() {
      it( 'should delete a LabelEvent without error', function( done ) {
        api.labelEvent.delete( labelEventFixture.ID ) .then ( function ( response ) {
        let data = response.data;
          done()
        } ).catch((ex) => {
            done( ex )
        } )
      } )
    } )
  } )

  describe( '.delete()', function() {
    it( 'should delete a Label without error', function( done ) {
      var fields = { Name: 'Another Label ' + uniqueKey }
      api.label.post(  fields ) .then ( function ( response ) {
        let data = response.data;
        var labelID = data.ID
        api.label.delete( labelID ) .then ( function ( response ) {
        let data = response.data;
          done()
        } ).catch((ex) => {
            done( ex )
        } )
      } )
    } )
  } )
} )
