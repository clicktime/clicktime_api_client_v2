var h = require('../helper');
var should = h.should;
var api = h.api.v2;

describe('v2/Me', function () {
  describe('.get()', function () {
    it('should retrieve my data without error', function (done) {
      api.me
        .get()
        .then(function (response) {
          let data = response.data;
          should.exist(data);
          should.exist(data.ID);
          should.exist(data.AuthToken);
          should.exist(data.AuthToken.Token);
          done();
        })
        .catch((ex) => {
          done(ex);
        });
    });
  });
  // Doesn't work
  // describe( '.services()', function() {
  //   it( 'should retrieve my data without error', function( done ) {
  //     api.me.services( ).then( function( response ) {
  //       let data = response.data;
  //       should.exist( data )
  //       done()
  //     } ).catch((ex) => {
  //       console.log(ex)
  //         done( ex )
  //     } )
  //   } )
  // } )
});
