var h = require('../helper')
var SUCCESS_CODES = h.SUCCESS_CODES
var should = h.should
var api = h.api.v2
var uniqueKey = h.uniqueKey

describe( 'v2/Clients', function() {
  var clientFixture = null
  describe( '.getAll()', function() {
    it( 'should retrieve Clients without error', function( done ) {
      api.client.getAll( { IsActive: true } ) .then (function (response) {
        let data = response.data;
        should.exist( data )
        should.exist( data[0].ID )
        clientFixture = data[0]
        done()
      } ).catch((ex) => {
          done( ex )
      } )
    } )
  } )
  describe( '.get()', function() {
    it( 'should retrieve a Client without error', function( done ) {
      api.client.get( clientFixture.ID ) .then (function (response) {
        let data = response.data;
        should.exist( data )
        should.exist( data.ID )
        data.ID.should.equal( clientFixture.ID )
        done()
      } ).catch((ex) => {
          done( ex )
      } )
    } )
  } )
  describe( '.post()', function() {
    var fields = {
      ClientNumber: null,
      ShortName: 'Short Name ' + uniqueKey,
      Notes: null,
      AccountingPackageID: null,
      Name: 'Client Name ' + uniqueKey,
      IsActive: true
    }
    it( 'should create a Client without error', function( done ) {
      api.client.post(  fields ) .then (function (response) {
        let data = response.data;
        should.exist( data )
        should.exist( data.ID )
        should.exist( data.Name )
        clientFixture = data
        done()
      } ).catch((ex) => {
          done( ex )
      } )
    } )
  } )
  describe( '.patch()', function() {
    var updates = { Name: 'Test' + uniqueKey }
    it( 'should update a Client without error', function( done ) {
      api.client.patch( clientFixture.ID, updates ) .then (function (response) {
        let data = response.data;
        should.exist( data )
        should.exist( data.ID )
        should.exist( data.Name )
        clientFixture = data
        data.Name.should.equal( updates.Name )
        done()
      } ).catch((ex) => {
          done( ex )
      } )
    } )
  } )
  describe( '.delete()', function() {
    it( 'should delete a Client without error', function( done ) {
      api.client.delete( clientFixture.ID ) .then (function (response) {
        done()
      } ).catch((ex) => {
          done( ex )
      } )
    } )
  } )
} )
