var h = require('../helper')
var SUCCESS_CODES = h.SUCCESS_CODES
var should = h.should
var api = h.api.v2
var uniqueKey = h.uniqueKey

describe( 'v2/Jobs', function() {
  var jobFixture = null
  describe( '.getAll()', function() {
    it( 'should retrieve Jobs without error', function( done ) {
      api.job.getAll( { IsActive: true } ) .then ( function ( response ) {
        let data = response.data;
        should.exist( data )
        should.exist( data[0].ID )
        jobFixture = data[0]
        done()
      } ).catch((ex) => {
        done( ex )
      } )
    } )
  } )
  describe( '.get()', function() {
    it( 'should retrieve a Job without error', function( done ) {
      api.job.get( jobFixture.ID ) .then ( function ( response ) {
        let data = response.data;
        should.exist( data )
        should.exist( data.ID )
        data.ID.should.equal( jobFixture.ID )
        done()
      } ).catch((ex) => {
        done( ex )
      } )
    } )
  } )
  describe( '.post()', function() {
    it( 'should create a Job without error', function( done ) {
      var fields = {
        JobNumber: 'TJOB' + uniqueKey,
        ClientID: jobFixture.ClientID,
        Name: 'Test Job' + uniqueKey
      }
      api.job.post(  fields ) .then ( function ( response ) {
        let data = response.data;
        should.exist( data )
        should.exist( data.ID )
        jobFixture = data
        done()
      } ).catch((ex) => {
        done( ex )
      } )
    } )
  } )
  describe( '.patch()', function() {
    it( 'should add a sync id to a Job without error', function( done ) {
      var updates = {
        Name: 'New Test Name' + uniqueKey
      }
      api.job.patch( jobFixture.ID, updates ) .then ( function ( response ) {
        let data = response.data;
        should.exist( data )
        should.exist( data.ID )
        data.Name.should.equal( updates.Name )
        done()
      } ).catch((ex) => {
        done( ex )
      } )
    } )
  } )
  describe( 'delete()', function() {
    it( 'should delete a Job without error', function( done ) {
      api.job.delete( jobFixture.ID ) .then ( function ( response ) {
        done()
      } ).catch((ex) => {
          done( ex )
      } )
    } )
  } )
} )
