var util = require('util');
var axios = require('axios');

/**
 * Format function arguments for output.
 * @param  {Object} args Function arguments to be formatted.
 * @return {string}    Formatted arguments.
 */
module.exports.formatArgs = function formatArgs(args) {
	return [util.format.apply(util.format, Array.prototype.slice.call(args))];
};

/**
 * Checks for and handles pagination
 * @param {Object}    Options for request
 * @return {Promise}  Returns a Promise for an array of all offsets given the service x-count
 */
module.exports.fetchAll = async function fetchAll(opts) {
	// Update method from wrapper terms 'GETALL' to API terms 'GET'
	opts.method = 'GET';

	// Initialize with first 'GET' to access headers for x-count
	try {
		const response = await axios(opts);
		let headers = response.data.page;
		const defaultLimit = opts.url === 'https://api.clicktime.com/v2/Reports/Time'
  			? 2500
  			: 100;
		const limit = headers.limit || defaultLimit;
		let currentPagination = 0;
		let promArr = [0];

		// Generate array of offset options
		while (headers.count >= 0) {
			currentPagination += limit;
			headers.count -= limit;
			promArr.push(currentPagination);
		}

		// Generate array of Promise requests
		promArr = promArr.map(async (offset) => {
			if (!opts.params) {
				opts.params = {};
			}
			opts.params['offset'] = offset;
			const { response } =  await axios(opts)
			return response.data;
		});
		return Promise.all(promArr)
	} catch (err) {
		throw err;
	}
};
