var fetchAll = require('./helpers').fetchAll;
var axios = require('axios');

module.exports = function (config) {
	'use strict';

	return async function (opts) {
		var startTime = new Date().getTime();
		console.debug('CLICKTIME', opts.method, opts.url, opts.params, opts.data);

		if (!opts.headers) {
			opts.headers = {};
		}
		opts.headers['Accept'] = 'application/json';
		opts.headers['Content-Type'] = 'application/json';
		opts.headers['User-Agent'] =
			'clicktime_api_client / 0.6.0' +
			(config.userAgentComment ? ' (' + config.userAgentComment + ')' : '');
		opts.headers['X-Browser'] = config.xBrowser || config.credentials.xBrowser;

		if (config.token) {
			opts.headers['Authorization'] = 'Token ' + config.token;
		} else {
			opts.headers['Authorization'] =
				'Basic ' +
				new Buffer(config.credentials.email + ':' + config.credentials.password).toString('base64');
		}

		// Validate the body
		switch (opts.method) {
			case 'FETCHALL':
				return fetchAll(opts);
			case 'BATCH':
				//No body type validation
				if (!opts.body) {
					throw new Error('Invalid request body. Please review your structure.');
				}
				try {
					const response = await axios(opts);
					console.debug(
						'CLICKTIME RESPONSE - SUCCESS: ',
						(new Date().getTime() - startTime).toString() + 'ms'
					);
					return response.data;
				} catch (error) {
					console.debug(
						'CLICKTIME RESPONSE - ERROR: ',
						(new Date().getTime() - startTime).toString() + 'ms'
					);
					return error;
					}
			case 'GET':
			case 'DELETE':
				// No body validation
				break;
			case 'POST':
			case 'PATCH':
				if (!opts.data || !(opts.data instanceof Object)) {
					throw new Error('Invalid request data. Please review your structure.');
				}
				opts.data = JSON.stringify(opts.data);
				break;
			default:
				throw new Error('Invalid or missing param {method}');
		}
		const response = await axios(opts);
		console.debug(
			'CLICKTIME RESPONSE - SUCCESS: ',
			(new Date().getTime() - startTime).toString() + 'ms'
		);
		var body = response.data;
		return body;
	};
};
