/**
 * User API.
 * @param  {function} requestor Responsible for HTTP(S) requests.
 * @param  {string}   url       API base URI.
 * @return {Object}             API response.
 */
module.exports = function (requestor, url) {
    'use strict';

    var resource = 'Users/';

    return {

        /**
         * Retrieve a list of Users.
         * @param  {Object}   opts     Request opts.
         * @return {Object}            API response.
         */
        fetchAll: function (params) {
            console.log('users');
            params = params || '';

            return requestor({
                method: 'FETCHALL',
                url: url + resource,
                params,
                followAllRedirects: true
            });
        },

        /**
         * Retrieve a list of Users.
         * @param  {Object}   opts     Request opts.
         * @return {Object}            API response.
         */
        getAll: function (params) {
            console.log('users');
            params = params || '';

            return requestor({
                method: 'GET',
                url: url + resource,
                params,
                followAllRedirects: true
            });
        },

        /**
         * Retrieve a User.
         * @param  {int}      id       User ID.
         * @return {Object}            API response.
         */
        get: function (id) {
            id = id || 0;

            return requestor({
                method: 'GET',
                url: url + resource + id,
                followAllRedirects: true
            });
        },

        /**
          * Update a Job.
          * @param  {int}      opts     Request opts.
          * @return {Object}            API response.
          */
        patch: function (id, data) {
            data = data || {};

            return requestor({
                method: 'PATCH',
                url: url + resource + id,
                followAllRedirects: true,
                data
            });
        }
    };
};
