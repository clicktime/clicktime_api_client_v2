/**
 * Division API.
 * @param  {function} requestor Responsible for HTTP(S) requests.
 * @param  {string}   url       API base URI.
 * @return {Object}             API response.
 */
module.exports = function (requestor, url) {
    'use strict';

    var resource = 'Divisions/';

    return {

        /**
         * Retrieve a list of Division.
         * @param  {Object}   opts     Request opts.
         * @return {Object}            API response.
         */
        fetchAll: function (params) {
            params = params || '';

            return requestor({
                method: 'FETCHALL',
                url: url + resource,
                params,
                followAllRedirects: true
            });
        },

        /**
         * Retrieve a list of Division.
         * @param  {Object}   opts     Request opts.
         * @return {Object}            API response.
         */
        getAll: function (params) {
            params = params || '';

            return requestor({
                method: 'GET',
                url: url + resource,
                params,
                followAllRedirects: true
            });
        },

        /**
         * Retrieve a Division.
         * @param  {int}      id       Division ID.
         * @return {Object}            API response.
         */
        get: function (id) {
            id = id || 0;

            return requestor({
                method: 'GET',
                url: url + resource + id,
                followAllRedirects: true
            });
        },

         /**
          * Create a Division.
          * @param  {int}      opts     Request opts.
          * @return {Object}            API response.
          */
        post: function (data) {
            data = data || {};

            return requestor({
                method: 'POST',
                url: url + resource,
                followAllRedirects: true,
                data
            });
        },

        /**
          * Update a Division.
          * @param  {string}   id       Division ID
          * @param  {int}      opts     Request opts.
          * @return {Object}            API response.
          */
        patch: function (id, data) {
            data = data || {};

            return requestor({
                method: 'PATCH',
                url: url + resource + id,
                followAllRedirects: true,
                data
            });
        },

        /**
          * Delete a Division.
          * @param  {string}   id           Division ID.
          * @return {Object}                API response.
          */
        delete: function (id) {
            return requestor({
                method: 'DELETE',
                url: url + resource + id,
                followAllRedirects: true
            });
        }
    };
};
