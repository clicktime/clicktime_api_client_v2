/**
 * LabelEvent API.
 * @param  {function} requestor Responsible for HTTP(S) requests.
 * @param  {string}   url       API base URI.
 * @return {Object}             API response.
 */
module.exports = function (requestor, url) {
    'use strict';

    var resource = 'LabelEvents/';

    return {
        /**
         * Retrieve a list of LabelEvents.
         * @param  {Object}   params   Request params.
         * @return {Object}            API response.
         */
        getAll: function (params) {
            params = params || '';

            return requestor({
                method: 'GET',
                url: url + resource,
                params,
                headers: {
                    'Content-type': 'application/json'
                },
                followAllRedirects: true
            });
        },

        /**
         * Create a LabelEvent.
         * @param  {Object}   data     Request body.
         * @return {Object}            API response.
         */
        post: function (data) {
            data = data || {};

            return requestor({
                method: 'POST',
                url: url + resource,
                followAllRedirects: true,
                data
            });
        },

        /**
         * Add IDs to an existing LabelEvent.
         * @param  {int}      id       LabelEvent ID
         * @param  {Array}    data     Request body.
         * @return {Object}            API response.
         */
        postToLabelEvent: function (id, data) {
            data = data || [];

            return requestor({
                method: 'POST',
                url: url + resource + id,
                followAllRedirects: true,
                data
            });
        },

        /**
          * Delete a LabelEvent.
          * @param  {int}      id       Label ID.
          * @return {Object}            API response.
          */
        delete: function (id) {
            return requestor({
                method: 'DELETE',
                url: url + resource + id,
                followAllRedirects: true
            });
        }
    };
};
