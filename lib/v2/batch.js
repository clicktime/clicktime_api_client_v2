/**
 * Client API.
 * @param  {function} requestor Responsible for HTTP(S) requests.
 * @param  {string}   url       API base URI.
 * @return {Object}             API response.
 */
module.exports = function (requestor, url) {
    'use strict';

    var resource = '$batch';

    return {
         /**
          * Create a Client.
          * @param  {int}      opts     Request opts.
          * @return {Object}            API response.
          */
        batch: function (headers, data) {
            headers = headers || {};
            data = data || {};

            return requestor({
                method: 'BATCH',
                url: url + resource,
                headers: headers,
                data
            });
        }
    };
};
