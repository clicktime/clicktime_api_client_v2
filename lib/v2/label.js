/**
 * Label API.
 * @param  {function} requestor Responsible for HTTP(S) requests.
 * @param  {string}   url       API base URI.
 * @return {Object}             API response.
 */
module.exports = function (requestor, url) {
    'use strict';

    var resource = 'Labels/';

    return {

        /**
         * Retrieve a list of Labels.
         * @param  {Object}   opts     Request opts.
         * @return {Object}            API response.
         */
        fetchAll: function (params) {
            params = params || '';

            return requestor({
                method: 'FETCHALL',
                url: url + resource,
                params,
                followAllRedirects: true
            });
        },

        /**
         * Retrieve a list of Labels.
         * @param  {Object}   opts     Request opts.
         * @return {Object}            API response.
         */
        getAll: function (params) {
            params = params || '';

            return requestor({
                method: 'GET',
                url: url + resource,
                params,
                followAllRedirects: true
            });
        },

        /**
          * Create a Label.
          * @param  {int}      opts     Request opts.
          * @return {Object}            API response.
          */
        post: function (data) {
            data = data || {};

            return requestor({
                method: 'POST',
                url: url + resource,
                followAllRedirects: true,
                data
            });
        },

        /**
          * Delete a Label.
          * @param  {int}      id           Label ID.
          * @return {Object}                API response.
          */
        delete: function (id) {
            return requestor({
                method: 'DELETE',
                url: url + resource + id,
                followAllRedirects: true
            });
        }
    };
};
