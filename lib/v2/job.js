/**
 * Job API.
 * @param  {function} requestor Responsible for HTTP(S) requests.
 * @param  {string}   url       API base URI.
 * @return {Object}             API response.
 */
module.exports = function (requestor, url) {
    'use strict';

    var resource = 'Jobs/';

    return {

        /**
         * Retrieve a list of Jobs.
         * @param  {Object}   opts     Request opts.
         * @return {Object}            API response.
         */
        fetchAll: function (params) {
            params = params || '';

            return requestor({
                method: 'FETCHALL',
                url: url + resource,
                params,
                followAllRedirects: true
            });
        },

        /**
         * Retrieve a list of Jobs.
         * @param  {Object}   opts     Request opts.
         * @return {Object}            API response.
         */
        getAll: function (params) {
            params = params || '';

            return requestor({
                method: 'GET',
                url: url + resource,
                params,
                followAllRedirects: true
            });
        },

        /**
         * Retrieve a Job.
         * @param  {int}      id       Job ID.
         * @return {Object}            API response.
         */
        get: function (id) {
            id = id || 0;

            return requestor({
                method: 'GET',
                url: url + resource + id,
                followAllRedirects: true
            });
        },

        /**
          * Create a Job.
          * @param  {int}      opts     Request opts.
          * @return {Object}            API response.
          */
        post: function (data) {
            data = data || {};

            return requestor({
                method: 'POST',
                url: url + resource,
                followAllRedirects: true,
                data
            });
        },

        /**
          * Update a Job.
          * @param  {int}      opts     Request opts.
          * @return {Object}            API response.
          */
        patch: function (id, data) {
            data = data || {};

            return requestor({
                method: 'PATCH',
                url: url + resource + id,
                followAllRedirects: true,
                data
            });
        },

        /**
          * Delete a Job.
          * @param  {string}   id           Job ID.
          * @return {Object}                API response.
          */
        delete: function (id) {
            return requestor({
                method: 'DELETE',
                url: url + resource + id,
                followAllRedirects: true
            });
        }
    };
};
