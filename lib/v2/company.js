/**
 * Company API.
 * @param  {function} requestor Responsible for HTTP(S) requests.
 * @param  {string}   url       API base URI.
 * @return {Object}             API response.
 */
module.exports = function (requestor, url) {
    'use strict';

    var resource = 'Company';

    return {

        /**
         * Retrieve a Company.
         * @param  {int}      id       Company ID.
         * @return {Object}            API response.
         */
        get: function () {
            return requestor({
                method: 'GET',
                url: url + resource,
                followAllRedirects: true
            });
        }
    };
};
