/**
 * Me API.
 * @param  {function} requestor Responsible for HTTP(S) requests.
 * @param  {string}   url       API base URI.
 * @return {Object}             API response.
 */
module.exports = function (requestor, url) {
    'use strict';

    var resource = 'Me';

    return {

        /**
         * Retrieve a Me.
         * @return {Object}            API response.
         */
        get: function () {
            return requestor({
                method: 'GET',
                url: url + resource,
                followAllRedirects: true
            });
        },
        services: function () {
            return requestor({
                method: 'GET',
                url: url + resource + '/Services',
                followAllRedirects: true
            });
        }
    };
};
