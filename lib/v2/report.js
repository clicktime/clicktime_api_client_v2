/**
 * Report API.
 * @param  {function} requestor Responsible for HTTP(S) requests.
 * @param  {string}   url       API base URI.
 * @return {Object}             API response.
 */
module.exports = function (requestor, url) {
    'use strict';

    var resource_export = 'Reports/Time';

    return {
        /**
         * Retrieve a Report.
         * @return {Object}            API response.
         */
                /**
         * Retrieve a list of Division.
         * @param  {Object}   opts     Request opts.
         * @return {Object}            API response.
         */
        fetchTimeEntries: function (params) {
            params = params || '';

            return requestor({
                method: 'FETCHALL',
                url: url + resource_export,
                followAllRedirects: true,
                params
            });
        },

        timeEntries: function (params) {
            params = params || '';
            return requestor({
                method: 'GET',
                url: url + resource_export,
                followAllRedirects: true,
                params
            });
        }
    };
};
