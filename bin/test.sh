#!/bin/bash
ERROR=0
if [ -z "$EMAIL" ]; then
  echo "Please define EMAIL env variable."
  ERROR=1
fi
if [ -z "$PASSWORD" ]; then
  echo "Please define PASSWORD env variable."
  ERROR=1
fi
if [[ ! ERROR ]]; then
  echo "Missing configuration, can't run tests."
  exit
fi

mocha --timeout 30000 $TESTS
