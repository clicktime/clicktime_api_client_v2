# ClickTime API Client

A REST client for the new v2 API from ClickTime, with partial support for the legacy 1.3 API.

- **REST v2** - https://developer.clicktime.com/docs/api/
- **Legacy 1.3** - http://app.clicktime.com/api/1.3/help

### Requirements

- Node (tested on 6.x)
- NPM

### Installing

- Clone repo & cd into directory
- `npm install`

### Running Tests

```
EMAIL=your@email.com PASSWORD=yourpassword npm run test
```

EMAIL=ebeauchamp+test@clicktime.com PASSWORD=123 npm run test:v2


### Usage:

```
var ct = clicktimeApi({
	token: session.CT_Token,
}).v2;

ct.company.get()
	.then((response) => {
		console.log(response.data);
	})
	.catch((err) => {
		console.log(err);
	});
```