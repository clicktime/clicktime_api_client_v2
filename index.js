const formatArgs = require('./lib/helpers').formatArgs;

const V2_URI = 'https://api.clicktime.com/v2/';

/**
 * Bootstrap.
 * @param  {Object}   config                        Configuration (API credentials, debug, user agent)
 * @param  {String}   config.token                  Authentication Token
 * @param  {String}   config.credentials.email      Authentication Email
 * @param  {String}   config.credentials.password   Authentication Password
 * @param  {String}   config.userAgentComment       User Agent Comment
 * @param  {String}   config.uri                    API URI
 * @return {Object}   Current API lib information.
 */
module.exports = function( config ) {
  'use strict';

  if( !config ) { config = {}; }

  // Debug options.
  config.debug = config.debug || false;
  if( config.debug ) {
    console.debug = function() {
      return console.log.apply( console.log, formatArgs(arguments) );
    };
  } else {
    console.debug = function() {
      return void 0;
    };
  }

  // HTTP Basic Authentication.
  config.credentials = config.credentials || { email: '', password: '' };

  // Expose.
  const requestor = require('./lib/requestor')(config);

  const apiLib = {
    v2: {
      company: require('./lib/v2/company.js')(requestor, config.uri || V2_URI),
      batch: require('./lib/v2/batch.js')(requestor, config.uri || V2_URI),
      user: require('./lib/v2/user.js')(requestor, config.uri || V2_URI),
      division: require('./lib/v2/division.js')(requestor, config.uri || V2_URI),
      client: require('./lib/v2/client.js')(requestor, config.uri || V2_URI),
      job: require('./lib/v2/job.js')(requestor, config.uri || V2_URI),
      task: require('./lib/v2/task.js')(requestor, config.uri || V2_URI),
      label: require('./lib/v2/label.js')(requestor, config.uri || V2_URI),
      labelEvent: require('./lib/v2/label_event.js')(requestor, config.uri || V2_URI),
      me: require('./lib/v2/me.js')(requestor, config.uri || V2_URI),
      report: require('./lib/v2/report.js')(requestor, config.uri || V2_URI)
    },
    config: config,
    requestor: requestor
  };

  return apiLib;
};
